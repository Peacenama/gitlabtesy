package com.example.gitlabTest.gitlabDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabDemoApplication.class, args);
		System.out.println("hello-Peace this is GitLab");
	}

}
